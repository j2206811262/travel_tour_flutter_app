import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:travel_tour_flutter/model/data_model.dart';
class DataServices {
  String baseUrl = "http://mark.bslmeiyu.com/api";
  Future<List<DataModel>> getInfo() async {
    var apiUrl = "/getplaces";
    http.Response res = await http.get(Uri.parse(baseUrl + apiUrl));
    try {
      if (res.statusCode == 200) {
        List<dynamic> list = json.decode(res.body);
        var result = list.map((e) => DataModel.fromJson(e)).toList();
        print(result);
        return result;
      } else {
        return <DataModel>[];
      }
    } catch(e) {
      print(e);
      return <DataModel>[];
    }
  }
}